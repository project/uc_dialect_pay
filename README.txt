Welcome to Dialect Pay.

This module provides an interface to the Dialect MIGS (MasterCard Internet Gateway System) gateway service as a payment method for UberCart.

Currently supported institutions and payment models

ANZ Bank eGate - 2Party model - Installed

Planned institutions and payment models

ANZ Bank eGate - 3Party model - Coming
Commonwealth Bank CommWeb - 2Party model - Coming
Commonwealth Bank CommWeb - 3Party model - Coming

Models ~ 2Party or 3Party

2Party systems keep the customer on your pages. The transaction is handled immediately and the credit card details are encrypted and stored on your server. This has security implications for you.

3Party systems take the customer from your website to a hosted payment page provided by the banking institution. The customer enters their card details directly into the bank's systems. The advantage of this model is that you do not store your customer's card details.

To use this service you must have a valid Merchant account from one of the supported institutions.

Installation

Place this module in your sites/all/modules directory and enable it from the Modules administration screen. Note that you will have to have the UberCart Credit Card module enabled as well.

Setup

To enter your VPC Merchant account details go to admin/store/settings/payment/edit/gateways. Enter your Merchant ID, your Merchant Access Code and your Secure Secret Hash code. You obtain this information from Merchant Account administration system provided by your bank.

You can leave all other settings to their defaults at this time.

Navigate to admin/store/settings/payment/edit/methods. make sure that the Credit Card payment method is enabled and select Dialect Pay as the Default gateway from the popup menu.

Testing

Leave Dialect Pay in Test mode and do your initial testing to make sure that the module is providing the actions required for each of the test transactions as recommended by the Institution. 

See TESTING.txt for each bank's testing regime.

Payment Handling

If a transaction is successful, the following events happen
	- The Order has its status updated to 'payment received'.
	- The Order's log is updated with a reference to the payment. Note that the credit card number is recorded in the format Nxxx xxxx xxxx NNNN for security reasons.
	- If logging is enabled then the transaction is logged into Watchdog.

Going Live

You will have to contact your institution before going live. This will require you to make changes to your Merchant Account and Access Code details.





















